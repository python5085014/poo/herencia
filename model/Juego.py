from Mago import Mago
from Guerrero import Guerrero


def calcularPuntaje(personajes: list):
    """ Este método se encarga de calcular el puntaje de cada uno de los personajes

    Args:
        personajes (list): una lista de personajes ya sean Guerreros o Magos

    Returns:
        int : puntaje total relacionado a un tipo de personaje
    """
    return sum(personaje.puntaje for personaje in personajes)


def buscarGanador(puntajeGuerreros: int, puntajeMagos: int):
    """ Este método verifica el ganador del juego
    Args:
        puntajeGuerreros (int): puntaje total del equipo de guerreros.
        puntajeMagos (int): puntaje total del equipo de magos.
    """
    if puntajeGuerreros > puntajeMagos:
        print("El ganador es el equipo de : Guerreros")
    elif puntajeGuerreros < puntajeMagos:
        print("El ganador es el equipo de: Magos")
    else:
        print("Han empatado!!!")

        

# Crear instancias de guerreros y magos
equipo_guerreros = [
    Guerrero("Aragorn", 10, "Espada"),
    Guerrero("Legolas", 15, "Arco")
]

equipo_magos = [
    Mago("Gandalf", 7, "Bola de fuego"),
    Mago("Merlín", 12, "Rayo mágico")
]

# Realizar los ataques y lanzar los hechizos
print("----------------------ATAQUES Y HECHIZOS-------------------")
for guerrero in equipo_guerreros:
    guerrero.atacar()

for mago in equipo_magos:
    mago.lanzarHechizo()


puntajeGuerreros = calcularPuntaje(equipo_guerreros)
puntajeMagos = calcularPuntaje(equipo_magos)

print("------------------RESULTADOS------------------------------")
print('Puntaje de los Guerreros: ', calcularPuntaje(equipo_guerreros))
print('Puntaje de los Magos: ', calcularPuntaje(equipo_magos))
buscarGanador(puntajeGuerreros, puntajeMagos)