from Personaje import Personaje


class Guerrero(Personaje):
    def __init__(self, nombre: str, nivel: int, arma: str):
        super().__init__(nombre, nivel)
        self.arma = arma
        self.puntaje = 0

    def atacar(self):
        if (self.nivel < 5):
            self.puntaje += self.nivel + 3
        elif 6 <= self.nivel <= 10:
            self.puntaje += self.nivel + 5
        elif 11 <= self.nivel <= 100:
            self.puntaje += self.nivel + 10
        else:
            self.puntaje += 0

        print(f'{self.nombre} ataca con {self.arma}. Puntaje: {self.puntaje}')
