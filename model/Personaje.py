class Personaje:
    def __init__(self, nombre, nivel):
        self.nombre = nombre
        self.nivel = nivel

    def saludar(self):
        print(f'Hola mi nombre es {self.nombre} y soy de nivel {self.nivel}')