from Personaje import Personaje

class Mago(Personaje):
    def __init__(self, nombre: str, nivel: int, hechizo: str):
        super().__init__(nombre, nivel)
        self.hechizo = hechizo
        self.puntaje = 0

    def lanzarHechizo(self):
        if (self.nivel < 5):
            self.puntaje += self.nivel + 5
        elif 6 <= self.nivel <= 10:
            self.puntaje += self.nivel + 8
        elif 11 <= self.nivel <= 100:
            self.puntaje += self.nivel + 12
        else:
            self.puntaje += 0
        print(f'{self.nombre} lanza el hechizo {self.hechizo}. Puntaje: {self.puntaje}')