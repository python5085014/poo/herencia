# Herencia Simple

## Las clases hijos heredan atributos y métodos de las clases padres.

<hr/>

## Problema

- En un juego de rol, hay un equipo de guerreros y un equipo de magos que están a punto de enfrentarse en una batalla. Cada guerrero tiene un nivel de ataque y cada mago tiene un nivel de hechizo. El equipo que tenga la suma más alta de los niveles de ataque de los guerreros y los niveles de hechizo de los magos ganará la batalla.
- Escribe una función llamada **"calcularPuntaje"** que tome como parámetros una lista de objetos Guerrero y una lista de objetos Mago, y devuelva el equipo ganador (guerreros o magos) basado en el puntaje total.
- Recuerda que la clase Guerrero tiene el método "atacar" que devuelve el nivel de ataque y la clase Mago tiene el método "lanzar_hechizo" que devuelve el nivel de hechizo.
- Los personajes tendran diferentes puntajes dependiendo del nivel en que se encuentren y al tipo de personaje al que pertenescan por ejemplo:

  | Tipo de Personaje | Nivel          | Puntaje    |
  | ----------------- | -------------- | ---------- |
  | Guerrero          | menor a 5      | nivel + 3  |
  | Guerrero          | entre 6 y 10   | nivel + 5  |
  | Guerrero          | entre 11 y 100 | nivel + 10 |
  | Guerrero          | otro valor     | 0          |
  | Mago              | menor a 5      | nivel + 5  |
  | Mago              | entre 6 y 10   | nivel + 8  |
  | Mago              | entre 11 y 100 | nivel + 12 |
  | Mago              | otro valor     | 0          |

<br/>    
<hr/>

## Solución: **Diagrama de Clases**

![Descripción de la imagen](DC.PNG)
